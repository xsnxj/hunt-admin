package com.hunt.support.impl;

import com.fasterxml.jackson.core.JsonGenerator;
import com.google.gson.Gson;
import com.hunt.model.ALiBaBa.ALiDaYu.ALiDaYiSendMessageSuccessResponseRoot;
import com.hunt.model.ALiBaBa.ALiDaYu.ALiDaYuSendMessageErrorResponseRoot;
import com.hunt.model.ALiBaBa.ALiDaYu.ALiDaYuSendMessageParam;
import com.hunt.service.SystemService;
import com.hunt.support.TaoBaoService;
import com.hunt.util.HttpUtil;
import com.hunt.util.OathUtil;
import com.hunt.util.SystemConstant;
import org.apache.shiro.cache.Cache;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.KeyBoundCursor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * @Author ouyangan
 * @Date 2017/1/13/15:49
 * @Description
 */
@Service
@Transactional
public class TaoBaoServiceImpl implements TaoBaoService {

    private static final Logger log = LoggerFactory.getLogger(TaoBaoServiceImpl.class);

    @Autowired
    private SystemService systemService;

    /**
     * @param phone 号码
     * @param param 参数
     * @Author ouyangan
     * @Date 2017-1-13 15:43:56
     * @Description 短信验证码
     */
    public void aLiDaYuSendMessage(String phone, Map<String, String> map) throws Exception {
        String url = systemService.selectDataItemByKey(SystemConstant.aLiDaYuRegisterUrl, 5L);
        String secret = systemService.selectDataItemByKey(SystemConstant.aLiDaYuRegisterSecret, 5L);
        String key = systemService.selectDataItemByKey(SystemConstant.aLiDaYuRegisterAppKey, 5L);
        String templateSign = systemService.selectDataItemByKey(SystemConstant.aLiDaYuRegisterTemplateSign, 5L);
        String code = systemService.selectDataItemByKey(SystemConstant.aLiDaYuRegisterTemplateCode, 5L);
        aLiDaYuSendMessage(phone, map, url, secret, key, templateSign, code);
    }

    /**
     * 发送短信
     *
     * @param phone
     * @param map
     * @param url
     * @param secret
     * @param key
     * @param templateSign
     * @param templateCode
     * @throws Exception
     */
    private void aLiDaYuSendMessage(String phone, Map<String, String> map, String url, String secret, String key, String templateSign, String templateCode) throws Exception {
        ALiDaYuSendMessageParam message = buildBasicParam(key, templateSign, templateCode);
        message.setRec_num(phone);
        message.setSms_param(new Gson().toJson(map));
        Map<String, String> param = OathUtil.taoBaoSignature(message, secret);
        String post = HttpUtil.post(url, param);
        log.debug("阿里大于,短信,响应.{}", post);
        ALiDaYiSendMessageSuccessResponseRoot successResponse = new Gson().fromJson(post, ALiDaYiSendMessageSuccessResponseRoot.class);
        System.out.println(successResponse);
        if (successResponse.getAlibaba_aliqin_fc_sms_num_send_response() == null || !successResponse.getAlibaba_aliqin_fc_sms_num_send_response().getResult().getSuccess()) {
            ALiDaYuSendMessageErrorResponseRoot errorResponse = new Gson().fromJson(post, ALiDaYuSendMessageErrorResponseRoot.class);
            throw new Exception("阿里大于.短信发送失败.失败原因:" + errorResponse.getError_response().getSub_msg());
        }
    }

    /**
     * 构建基础参数
     *
     * @param key
     * @param templateSign
     * @param templateCode
     * @return
     */
    private ALiDaYuSendMessageParam buildBasicParam(String key, String templateSign, String templateCode) {
        ALiDaYuSendMessageParam message = new ALiDaYuSendMessageParam();
        message.setMethod("alibaba.aliqin.fc.sms.num.send");
        message.setApp_key(key);
        message.setSign_method("md5");
        message.setTimestamp(DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        message.setFormat("json");
        message.setV("2.0");
        message.setSms_type("normal");
        message.setSms_free_sign_name(templateSign);
        message.setSms_template_code(templateCode);
        return message;
    }

}
